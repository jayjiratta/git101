def grade_for_change(grade):
    if grade == 'A' :
        return 4
    elif grade == 'B+' :
        return 3.5
    elif grade == 'B' :
        return 3
    elif grade == 'C+' :
        return 2.5
    elif grade == 'C' :
        return 2
    elif grade == 'D+' :
        return 1.5
    elif grade == 'D' :
        return 1
    elif grade == 'E' :
        return 0
    else :
        print('error')

print("Enter grade (A-E) for each course")

P = 'Physics I'
print(P)
credit_P = 3
phy_grade_alpha = (input("grade = "))
phy_grade_digit = grade_for_change(phy_grade_alpha)
phy_section = int(input("section = "))

M = 'Math I'
print(M)
credit_M = 3
math_grade_alpha = (input("grade = "))
math_grade_digit = grade_for_change(math_grade_alpha)
math_section = int(input("section = "))

C = 'Chemistry'
print(C)
credit_C = 3
chem_grade_alpha = (input("grade = "))
chem_grade_digit = grade_for_change(chem_grade_alpha)
chem_section = int(input("section = "))

IC = 'Intro to Computer Prog'
print(IC)
credit_IC = 3
intro_grade_alpha = (input("grade = "))
intro_grade_digit = grade_for_change(intro_grade_alpha)
intro_section = int(input("section = "))

T = 'Table Tennis'
print(T)
credit_T = 1
pingpong_grade_alpha = (input("grade = "))
pingpong_grade_digit = grade_for_change(pingpong_grade_alpha)
pingpong_section = int(input("section = "))

#calculating
up = (credit_P*phy_grade_digit)+(credit_M*math_grade_digit)+(credit_C*chem_grade_digit)+(credit_IC*intro_grade_digit)+(credit_T*pingpong_grade_digit)
low = credit_P+credit_M+credit_C+credit_IC+credit_T
grade = up/low

print("")
print(" GRADE REPORT")
print("------------------------------------------------------")
print("#  {:20} {:^10} {:^10} {:^10}".format('Course','Section','Credit','Grade'))
print("------------------------------------------------------")
print("1  {:<23} {:02d}           {:<9} {:<2}".format(P,phy_section,credit_P,phy_grade_alpha))
print("2  {:<23} {:02d}           {:<9} {:<2}".format(M,math_section,credit_M,math_grade_alpha))
print("3  {:<23} {:02d}           {:<9} {:<2}".format(C,chem_section,credit_C,chem_grade_alpha))
print("4  {:<23} {:02d}           {:<9} {:<2}".format(IC,intro_section,credit_IC,intro_grade_alpha))
print("5  {:<23} {:02d}           {:<9} {:<2}".format(T,pingpong_section,credit_T,pingpong_grade_alpha))
print("------------------------------------------------------")
print("")
print(" GPA = %.2f"%(grade))
print("--------------------")
# d 1
# c 2
# b 3
# a 4
